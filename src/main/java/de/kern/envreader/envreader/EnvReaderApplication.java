package de.kern.envreader.envreader;

import de.kern.envreader.envreader.config.TestConfigProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(TestConfigProperties.class)
public class EnvReaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(EnvReaderApplication.class, args);
    }
}
