package de.kern.envreader.envreader.web;

import de.kern.envreader.envreader.config.TestConfigProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Properties;

@RestController
public class EnvController {

	private final TestConfigProperties configProps;

	@Autowired
	public EnvController(TestConfigProperties configProps) {
		this.configProps = configProps;
	}

	@GetMapping(value = "/environment", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public HttpEntity<Map<String, String>> getEnvironment() {
		return ResponseEntity.ok(System.getenv());
	}

	@GetMapping(value = "/configproperties", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public HttpEntity<TestConfigProperties> getConfigProperties() {
		return ResponseEntity.ok(configProps);
	}

	@GetMapping(value = "/systemproperties", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public HttpEntity<Properties> getSystemProperties() {
		return ResponseEntity.ok(System.getProperties());
	}

}
