package de.kern.envreader.envreader.config;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("testconfig")
public class TestConfigProperties {

    private String availableOnSystem;
    private String unAvailable;

}
