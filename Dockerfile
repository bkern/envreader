FROM frolvlad/alpine-oraclejdk8:slim

WORKDIR /app/

EXPOSE 50000

#ENV vars should be provided by the stack.yml file. This is just for local testing
ENV TEST_VARIABLE = "it works"

COPY target/envreader*.jar app.jar

ENTRYPOINT ["java", "-jar", "app.jar"]
